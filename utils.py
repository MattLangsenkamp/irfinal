import pickle
import subprocess

from pyserini.search.lucene import LuceneImpactSearcher
from pyserini.search.lucene import LuceneSearcher
from pyserini.query_iterator import get_query_iterator, TopicsFormat
from typing import List, Dict
from domain import Result, ResultPair
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
import statistics


def calculate_stats(filename):
    print(f"reading results from {filename}...\n")
    reread_results = load_results(filename)
    bm25_runtime_total = [x.bm25_runtime for x in reread_results]
    splade_runtime_total = [x.splade_runtime for x in reread_results]

    bm25_stats = {
        "mean": statistics.mean(bm25_runtime_total),
        "variance": statistics.variance(bm25_runtime_total),
        "std_dev": statistics.stdev(bm25_runtime_total)
    }

    splade_stats = {
        "mean": statistics.mean(splade_runtime_total),
        "variance": statistics.variance(splade_runtime_total),
        "std_dev": statistics.stdev(splade_runtime_total)
    }

    print("----------Data in seconds----------")

    print("System\t\tMean\t\tVariance\tStandar Deviation")
    print(f"BM25\t\t{bm25_stats['mean']:.4f}\t\t{bm25_stats['variance']:.4f}\t\t{bm25_stats['std_dev']:.4f}")
    print(f"SPLADE\t\t{splade_stats['mean']:.4f}\t\t{splade_stats['variance']:.4f}\t\t{splade_stats['std_dev']:.4f}")
    print("-----------------------------------")


def get_splade_searcher():
    return LuceneImpactSearcher.from_prebuilt_index(
        'msmarco-v1-passage-distill-splade-max',
        'naver/splade-cocondenser-ensembledistil')


def get_bm25_searcher():
    return LuceneSearcher.from_prebuilt_index('msmarco-v1-passage')


def get_topics(exp):
    if exp == "msmarco":
        return get_query_iterator("msmarco-passage-dev-subset", TopicsFormat.DEFAULT)
    else:
        return get_query_iterator("queries/msmarco-test2019-queries.tsv", TopicsFormat.DEFAULT)


def search_top_k(query: str, searcher, k) -> List[Result]:
    results = searcher.search(query, k)[:k]
    max = -1
    min = results[0].score
    for item in results:
        if item.score > max:
            max = item.score
        if item.score < min:
            min = item.score
    range = max - min
    for item in results:
        item.score = (item.score - min) / range
    return [convert_result(r) for r in results]


def convert_result(r) -> Result:
    return Result(text=r.contents, lucene_doc_id=r.lucene_docid, doc_id=r.docid, score=r.score)


def insert_sorted_list(results: List[Result], new_item: Result):
    for i, item in enumerate(results):
        if item.score < new_item.score:
            results.insert(i, new_item)
            return
    results.append(new_item)


def interpolate_top_k(ranking_bm25: List[Result], ranking_splade: List[Result], alpha: float, k: int) -> List[Result]:
    assert 0 <= alpha <= 1
    ranking_splade_dic = {}
    ranking_bm25_dic = {}
    merged_ids = set()
    for item in ranking_bm25:
        merged_ids.add(item.doc_id)
        ranking_bm25_dic[item.doc_id] = item
    for item in ranking_splade:
        merged_ids.add(item.doc_id)
        ranking_splade_dic[item.doc_id] = item

    new_ranking = []
    for doc_id in merged_ids:

        score1 = ranking_bm25_dic[doc_id].score if doc_id in ranking_bm25_dic else 0
        score2 = ranking_splade_dic[doc_id].score if doc_id in ranking_splade_dic else 0

        new_score = alpha * score1 + (1 - alpha) * score2
        if doc_id in ranking_bm25_dic:
            text = ranking_bm25_dic[doc_id].text
            lucene_doc_id = ranking_bm25_dic[doc_id].lucene_doc_id
        else:
            text = "could not retrieve text from splade"
            lucene_doc_id = ranking_splade_dic[doc_id].lucene_doc_id

        insert_sorted_list(
            new_ranking,
            Result(text=text, score=new_score, doc_id=doc_id, lucene_doc_id=lucene_doc_id))
    return new_ranking[:k]


def write_sorted_list(results: List[Result], run_name: str, qid: int) -> str:
    out_str = ""
    for i, result in enumerate(results):
        # qid <tab> 0 <tab> docid <tab> rank <tab> score <tab> run name <newline>
        out_str = out_str + f"{qid}\t{0}\t{result.doc_id}\t{i}\t{result.score}\t{run_name}\n"
    return out_str


def create_interpolated_graph(interpolated: Dict[float, float], metric_used: str, outfile: str):
    plt.title(metric_used)
    # Set the x and y axis labels for the graph
    plt.xlabel("Alpha Value [0 = All Splade | 1 = All BM25]")
    plt.ylabel("Interpolated Scores")
    # Split the dictionary into x and y values for the graph
    xvals = list(interpolated.keys())
    xvals.sort()  # Ensure the keys are in increasing order from 0.0 to 1.0
    yvals = []
    for i in range(len(xvals)):
        yval = interpolated[xvals[i]]
        yvals.append(yval)
    # Plot the x and y values in a graph
    plt.plot(xvals, yvals)
    # Mark the max x y pair with a star
    plt.plot(xvals[np.argmax(yvals)], max(yvals), marker="*", markersize=10, markeredgecolor="black",
             markerfacecolor="yellow")
    # Mark the min x y pair with an x
    plt.plot(xvals[np.argmin(yvals)], min(yvals), marker="x", markersize=5, markeredgecolor="black",
             markerfacecolor="red")
    # Mark both of these in the legend
    plt.legend(handles=[
        Line2D([0], [0], color="white", marker="*", label='Max Interpolated Value', markeredgecolor="black",
               markerfacecolor="yellow", markersize=10),
        Line2D([0], [0], color="white", marker="x", label='Min Interpolated Value', markeredgecolor="black",
               markerfacecolor="red", markersize=10)])
    # Write graph to a pdf file
    plt.savefig(outfile)
    # Clear the figure
    plt.clf()


def create_latency_graph(bm25_times: List[float], splade_times: List[float], outfile: str):
    # Set the title of the graph
    plt.title("BM25 vs SPLADE Latency")
    # Set the x and y axis labels for the graph
    plt.xlabel("Query Number")
    plt.ylabel("Time in seconds")
    # Generate a list of x values for the given queries
    x_vals = range(1, len(bm25_times) + 1)
    # Plot the bm25 and splade times on the graph
    plt.plot(x_vals, bm25_times, label="BM25")
    plt.plot(x_vals, splade_times, label="SPLADE")
    # Create a legend for these values
    plt.legend()
    # Write graph to a pdf file
    plt.savefig(outfile)
    # Clear the figure
    plt.clf()


def save_results(results: List[ResultPair], outfile: str):
    with open(outfile, 'wb') as f:
        pickle.dump(results, f, protocol=pickle.HIGHEST_PROTOCOL)


def load_results(outfile: str) -> List[ResultPair]:
    with open(outfile, 'rb') as f:
        return pickle.load(f)


def get_metrics_results(alpha: float, exp) -> Dict[str, float]:
    if exp == "msmarco":
        f = 'qrels.dev.tsv'
    else:
        f = '2019qrels-pass.txt'
    output = subprocess.run([
        './trec_eval/trec_eval',
        '-m', 'map',
        '-m', 'ndcg',
        '-m', 'P.5,10',
        f'qrels/{f}',
        f'results/results_{alpha}.tsv'], stdout=subprocess.PIPE)
    metric_dict = {}
    for metric_line in output.stdout.decode("utf-8").strip().split('\n'):
        cells = [c.strip() for c in metric_line.split('\t')]
        metric_dict[cells[0]] = float(cells[2])
    return metric_dict
