import dataclasses
from typing import List


@dataclasses.dataclass
class Result:
    text: str
    score: float
    doc_id: str
    lucene_doc_id: int


@dataclasses.dataclass
class ResultPair:
    qid: int
    bm25: List[Result]
    splade: List[Result]
    bm25_runtime: float
    splade_runtime: float


