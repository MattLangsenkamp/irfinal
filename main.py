import os.path
import time
from tqdm import tqdm
from utils import *
import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
                    prog='IRFinal',
                    description='Entry point for IR final script')

    parser.add_argument("-k", "--topk", help="number of documents for each system to return")
    parser.add_argument("-t", "--topic", help="Topic file to use for getting results."
                                              " Accompanying QREL will be used. use either "
                                              "dl2019 or msmarco")
    parser.add_argument("-rq", "--requery", action='store_true')

    parsed_args = parser.parse_args()

    k = int(parsed_args.topk)
    topic_file = parsed_args.topic
    re_query = parsed_args.requery

    if re_query:
        splade_index: LuceneImpactSearcher = get_splade_searcher()
        bm25_index: LuceneSearcher = get_bm25_searcher()

        results: List[ResultPair] = []

        topics = get_topics(topic_file)

        for topic in tqdm(topics):
            qid = topic[0]
            query = topic[1]
            splade_start = time.time()
            hits_splade = search_top_k(query, splade_index, k)
            splade_end = time.time()
            bm25_start = time.time()
            hits_bm25 = search_top_k(query, bm25_index, k)
            bm25_end = time.time()
            results.append(ResultPair(
                qid=qid,
                bm25=hits_bm25,
                splade=hits_splade,
                bm25_runtime=bm25_end - bm25_start,
                splade_runtime=splade_end - splade_start)
            )

        os.makedirs(os.path.join("results"), exist_ok=True)

        save_results(results, os.path.join("results", f"results-{topic_file}.dat"))
    reread_results = load_results(os.path.join("results", f"results-{topic_file}.dat"))

    p_10_dict = {}
    p_5_dict = {}
    map_dict = {}
    ndcg_dict = {}
    for alpha in np.arange(0, 1.1, 0.1):
        rounded_alpha = round(alpha, 1)
        with open(os.path.join("results", f"results_{rounded_alpha}.tsv"), "w") as f:
            print(f"Processing alpha {rounded_alpha}...")
            for query_pair in tqdm(reread_results):
                interpolated_results = interpolate_top_k(
                    query_pair.bm25,
                    query_pair.splade,
                    alpha=rounded_alpha,
                    k=k)
                f.write(write_sorted_list(interpolated_results, f"alpha_{rounded_alpha}", query_pair.qid))

        res = get_metrics_results(rounded_alpha, topic_file)
        map_dict[rounded_alpha] = res['map']
        ndcg_dict[rounded_alpha] = res['ndcg']
        p_5_dict[rounded_alpha] = res['P_5']
        p_10_dict[rounded_alpha] = res['P_10']

    create_interpolated_graph(map_dict, 'map', f'map-{topic_file}.pdf')
    print(map_dict)
    create_interpolated_graph(ndcg_dict, 'nDCG', f'nDCG-{topic_file}.pdf')
    print(ndcg_dict)
    create_interpolated_graph(p_5_dict, 'P@5', f'P_5-{topic_file}.pdf')
    print(p_5_dict)
    create_interpolated_graph(p_10_dict, 'P@10', f'P_10-{topic_file}.pdf')
    print(p_10_dict)

    bm25_times = []
    splade_times = []
    for result_pair in reread_results:
        bm25_times.append(result_pair.bm25_runtime)
        splade_times.append(result_pair.splade_runtime)

    create_latency_graph(bm25_times=bm25_times, splade_times=splade_times, outfile='latency.pdf')
    calculate_stats(os.path.join("results", f"results-{topic_file}.dat"))

