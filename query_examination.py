from pyserini.search import LuceneSearcher, LuceneImpactSearcher
from utils import get_topics, get_bm25_searcher, load_results, get_splade_searcher
import os

if __name__ == "__main__":

    topics = get_topics("dl2019")
    bm25_index: LuceneSearcher = get_bm25_searcher()
    splade_index: LuceneImpactSearcher = get_splade_searcher()

    reread_results = load_results(os.path.join("results", f"results-dl2019.dat"))

    for i, query in enumerate(topics):
        if i == 4:
            print(query)
            print("--------------------------------------------------------------------")
            print("--------------------------------------------------------------------")
            bm25_hits = bm25_index.search(query[1], 1000)
            for res in bm25_hits[:2]:
                print(res.raw)
            for res in bm25_hits[998:]:
                print(res.raw)
            print("--------------------------------------------------------------------")
            print("--------------------------------------------------------------------")
            splade_hits = splade_index.search(query[1], 1000)
            for res in splade_hits[:2]:
                print(bm25_index.doc(res.docid).raw())
            for res in splade_hits[998:]:
                print(bm25_index.doc(res.docid).raw())
            break

