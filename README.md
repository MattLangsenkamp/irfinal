# IR Final

A simple experiment comparing the information signals of SPLADE and BM25

## Installation

Tested with python 3.8.16 and java 11.0.12

Run the following commands to set up a JDK if necessary
```bash
curl -s "https://get.sdkman.io" | bash # optional if already installed or if java 11 installed and set as default and maven is installed
sdk install maven
sdk install java 11.0.12-open
sdk use java 11.0.12-open
```

Run the following to install python requirements:
```bash
pip install -r requirements.txt
```

Run the following to download qrel files
```bash
./bin/install
```

## Code Modifications 

Code was not modified to run these experiment,
but rather the Pyserini toolkit was leveraged to set up experiments

The `main.py` file sets up the experiment and calls functions to query the pre-built indices.
These results are then cached, so they do not have to be continuously rerun. 

Interpolation is then performed and graphs are automatically built. Many of the functions are in the `utils.py`
file to keep `main.py` more concise.

## Running experiments

To run the experiments with the DL2019 topics and the MS MARCO topics run the following commands

```bash
python main.py -k 1000 -rq -t dl2019
python main.py -k 1000 -rq -t msmarco
```

to rerun interpolation without re-querying run the following commands'
```bash
python main.py -k 1000 -t dl2019
python main.py -k 1000 -t msmarco
```