"""
text = []
qids = []
for topic in topics:
    qids.append(str(topic[0]))
    text.append(topic[1])

print("getting splade results...")
start = time.time()
splade_results = splade_index.batch_search(queries=text, qids=qids, threads=10)
end = time.time()
print(f"time taken to get splade search results: {end-start}")

print("getting bm25 results...")
start = time.time()
bm25_results = splade_index.batch_search(queries=text, qids=qids, threads=8)
end = time.time()
print(f"time taken to get bm25 search results: {end - start}")
"""